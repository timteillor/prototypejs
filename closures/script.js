// function createCalcFunction(n) {
//   return function() {
//     console.log(1000 * n)
//   }
// }

// const call = createCalcFunction(42)

// function createIncrementor(n) {
//   return function(num) {
//     return n + num
//   }
// }

// const addOne = createIncrementor(1)
// const addTen = createIncrementor(10)

// console.log(addOne(10))
// console.log(addOne(41))

// console.log(addTen(10))
// console.log(addTen(41))

// function urlGenerator(doamin) {
//   return function(url) {
//     return `https://${url}.${doamin}`
//   }
// }

// const comUrl = urlGenerator('com')
// const ruUrl = urlGenerator('ru')

// console.log(comUrl('google'))
// console.log(comUrl('netflix'))

// console.log(ruUrl('yandex'))
// console.log(ruUrl('vkontakte'))

/**
 * Написать свою функцию bind
 * Пример работы:
 * 
 * function logPerson() {
 *  console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
 * }
 * 
 * const person1 = {name: 'Michael', age: 22, job: 'Frontend'}
 * const person2 = {name: 'Elena', age: 19, job: 'SMM'}
 * 
 * bind(person1, logPerson)
 * bind(person2, logPerson)
 */

function bind(ctx, fn) {
  return function(...args) {
    fn.apply(ctx, args)
  }
}

function logPerson() {
  console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
}

const person1 = {name: 'Michael', age: 22, job: 'Frontend'}
const person2 = {name: 'Elena', age: 19, job: 'SMM'}

bind(person1, logPerson)()
bind(person2, logPerson)()